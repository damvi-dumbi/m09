package m09.threads03;

public class ThreadSincronitzat implements Runnable
{
	private int[] array;
	private Object sincronitzador;
	public ThreadSincronitzat(int[] array, Object sincronitzador)
	{
		this.array = array;
		this.sincronitzador = sincronitzador;
	}
	
	private synchronized void escriureArray()
	{
		for(Integer integer : array)
			System.out.println(Thread.currentThread().getName() + " -> " + integer);
	}
	

	public static synchronized void escriureArrayEstatic(int[] array)
	{
		for(Integer integer : array)
			System.out.println(Thread.currentThread().getName() + " -> " + integer);
	}
	
	public void run()
	{
		//forma amb variable a sincronitzar compartida
//		synchronized(sincronitzador)
//		{
//			for(Integer integer : array)
//				System.out.println(Thread.currentThread().getName() + " -> " + integer);
//		}
		
		//funcio sincronitzada, per� no funciona perqu�
		//no �s la mateixa instancia de classe :(
		//escriureArray();
		//i aquest potser si?
		//siiiiiiiiii perqu� al ser est�tic �s com� a totes les inst�ncies.
		ThreadSincronitzat.escriureArrayEstatic(array);
	}
}
