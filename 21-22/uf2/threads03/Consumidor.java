package m09.threads03;

public class Consumidor implements Runnable
{
	Productor productor;

	public Consumidor(Productor productor)
	{
		this.productor = productor;
	}

	public void run()
	{
		try
		{
			while (true)
			{
				synchronized(productor)
				{
					//esperem a que hi hagi un element
					productor.wait();
				}
				
				// consumim l'element
				System.out.println(Thread.currentThread().getName() + " -> " + productor.getBuffer());
				productor.getBuffer().clear();
				
				synchronized(productor.consumit)
				{
					productor.consumit.notify();
				}
				
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println(Thread.currentThread().getName() + " i acabo de consumir.");
		}
	}

}
