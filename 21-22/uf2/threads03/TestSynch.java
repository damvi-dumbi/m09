package m09.threads03;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestSynch
{	
	public static void main(String[] args)
	{
		int laMartaVolUnaLongitudDiferent = 1000;
		int[] arrayChulaPichula = new int[laMartaVolUnaLongitudDiferent];
		for(int i = 0; i < laMartaVolUnaLongitudDiferent; i++)
			arrayChulaPichula[i] = i;
		
		Object sincronitzador = new Object();
		ExecutorService executor = Executors.newFixedThreadPool(2);
		executor.execute(new ThreadSincronitzat(arrayChulaPichula, sincronitzador));
		executor.execute(new ThreadSincronitzat(arrayChulaPichula, sincronitzador));
		
		executor.shutdown();
		try {
			executor.awaitTermination(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}
}
