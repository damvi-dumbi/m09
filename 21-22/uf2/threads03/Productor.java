package m09.threads03;

import java.util.ArrayList;

public class Productor implements Runnable
{
	ArrayList<Integer> bufferAConsumir;
	public final Object consumit = new Object();
	public Productor()
	{
		bufferAConsumir = new ArrayList<Integer>();
	}
	
	
	public void run()
	{
		try
		{
			for(int i = 0; i < 24; i++)
			{
				//produir element
				bufferAConsumir.add(i);
				synchronized(this)
				{
					//avisar d'element produit
					this.notify();
				}
				
				//esperar que es consumeixi
				//aix� �s una mica in�til perqu�
				//no estem aprofitant la paral�lelitzaci�
				//dels threads.
				//
				//Normalment per a fer la demostraci�
				//utilitzariem un Thread.sleep() per simular
				//que est� rebent les dades d'algun lloc.
				//sense �s d'un segon sem�for
				synchronized(consumit)
				{
					consumit.wait();
				}
			}
		}catch(InterruptedException e)
		{
			e.printStackTrace();
			System.out.println("Yo! Interrupci� al productor -> SAD.");
		}
		
	}
	
	public ArrayList<Integer> getBuffer()
	{
		return bufferAConsumir;
	}
}
