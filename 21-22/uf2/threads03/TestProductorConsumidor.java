package m09.threads03;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestProductorConsumidor
{
	public static void main(String[] args)
	{
		Productor productor = new Productor();
		
		ExecutorService executor = Executors.newFixedThreadPool(2);
		executor.execute(new Consumidor(productor));
		executor.execute(new Consumidor(productor));
		
		executor.shutdown();
		
		Thread productorThread = new Thread(productor);
		productorThread.run();
		
		
		try {
			productorThread.join();
			System.out.println("Acaba el productor");
			
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
			System.out.println("Acaben els consumidors");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
