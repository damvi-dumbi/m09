package m09.threads01;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class MainExecutor
{
	public static void main(String[] args)
	{
		ExecutorService executor = Executors.newCachedThreadPool();
		System.out.println("Inicio el main.");
		for(int i=0;i<15;i++)
			executor.execute(new ElMeuPrimerThread());		
		
		System.out.println("Aturem m�s execucions.");
		executor.shutdown(); //aturem m�s execucions
		
		System.out.println("Fem una execuci� extra.");
		try {
			executor.execute(new ElMeuPrimerThread());
		}catch(RejectedExecutionException e)
		{
			System.out.println("No puc executar m�s coses.");
		}
		
		try {
			Thread.sleep(500);
			System.out.println("Aturem les execucions.");
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		System.out.println("Acabo el main.");
	}
}