package m09.threads01;

public class MainThreads
{
	public static void main(String[] args)
	{
		ElMeuPrimerThread elRunnable = new ElMeuPrimerThread();
		Thread elThread = new Thread(elRunnable);
		System.out.println("S�c el main, creo un thread.");
		elThread.start();
		try {
			System.out.println("S�c el main, espero al thread.");
			elThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("S�c el main, acabo.");
	}
}
