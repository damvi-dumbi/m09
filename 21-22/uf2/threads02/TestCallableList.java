package m09.threads02;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestCallableList
{
	public static void main(String[] args)
	{
		ExecutorService executor = Executors.newFixedThreadPool(10);
		System.out.println("llan�o el thread");
		ArrayList<Callable<Integer>> llista = new ArrayList<Callable<Integer>>();
		for(int i = 0; i < 20; i++)
			llista.add(new Sumador());
		
		try {
			ArrayList<Future<Integer>> resultat = (ArrayList<Future<Integer>>) executor.invokeAll(llista);
			executor.shutdown();
		
			System.out.println("Esperant que acabin els threads.");
			
			System.out.print("Els resultats son: ");
			for(Future<Integer> resultatIndividual: resultat)
				System.out.print(resultatIndividual.get()+ ", ");
			System.out.println();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}		
	}

}
