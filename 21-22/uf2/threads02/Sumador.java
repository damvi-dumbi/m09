package m09.threads02;

import java.util.concurrent.Callable;

public class Sumador implements Callable<Integer>
{

	public Integer call() throws Exception
	{
		int a = (int)(Math.random()*1000);
		int b = (int)(Math.random()*1000);
		System.out.println("els nombres s�n: " + a + " , " + b);
		Thread.sleep(5000);
		
		return a+b;
	}

}
