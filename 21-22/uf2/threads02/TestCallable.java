package m09.threads02;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestCallable
{
	public static void main(String[] args)
	{
		ExecutorService executor = Executors.newFixedThreadPool(10);
		System.out.println("llan�o el thread");
		Future<Integer> resultat = executor.submit(new Sumador());
		executor.shutdown();
		
		try {
			System.out.println("Esperant que acabi el thread.");
			System.out.println("El resultat �s:" + resultat.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

}
