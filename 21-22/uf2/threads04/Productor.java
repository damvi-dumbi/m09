package m09.threads04;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class Productor implements Callable<Boolean>
{
	ArrayList<Object> cosesProduides = new ArrayList<Object>();
	
	public synchronized Object donarDadaAConsumir()
	{
		if(cosesProduides.size() > 0)
		{
			//si dos threads arriben aqu� amb la funci�
			//sense haver estat sincronitzada.
			//Podem fer Out of Bounds.
			
//			Thread.sleep((long) (Math.random()*10));
			return cosesProduides.remove(0);
		}
		
		
		return null;
	}
	
	public Boolean call()
	{
		//produeixo les meves cosetes
		int cosesAProduir = (int) (Math.random()*10)+1;
		for(int i = 0; i < cosesAProduir; i++)
			cosesProduides.add(new Object());
		
		//avisem que hem produit les cosetes
		synchronized(this)
		{
			this.notifyAll();
		}
		
		//ja estem perqu� per la demo �s suficient
		return true;
	}

}
