package m09.threads04;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestProdCons
{
	public static void main(String[] args)
	{
		ExecutorService executor = Executors.newCachedThreadPool();
		
		Productor productor = new Productor();
		
		for(int i = 0; i < 15; i++)
			executor.submit(new Consumidor(productor));
		
		executor.submit(productor);
		executor.shutdown();
	}
}
