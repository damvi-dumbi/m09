package m09.threads04;

import java.util.concurrent.Callable;

public class Consumidor implements Callable<Boolean>
{
	Productor productor;
	
	public Consumidor(Productor productor)
	{
		this.productor = productor;
	}

	public Boolean call()
	{
		try
		{
			//esperem
			synchronized(productor)
			{
				productor.wait();
			}
			
			//rebem element
			Object cosaAConsumir = productor.donarDadaAConsumir();
			
			if(cosaAConsumir == null)
			{
				System.out.println(Thread.currentThread().getName() + ": M'he quedat sense elements.");
				return false;
			}
				
			//consumim element
//			Thread.sleep(50);
			System.out.println(Thread.currentThread().getName() + ": He consumit l'element.");
		}catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		return true;
	}

}