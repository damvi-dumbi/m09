package m09.music;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.Instrument;

public class TestMusic
{
	public static void main(String[] args)
	{
		Note[] arpegio = 
			{
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.D3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.G3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.D4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.G4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.D5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.G5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.D6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.G6, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.C7, Note.Duration.semicorchea),
				new Note(Note.Frequency.G6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.D6, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.G5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.D5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.G4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.D4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.G3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.D3, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.A2, Note.Duration.semicorchea),
				new Note(Note.Frequency.B2, Note.Duration.semicorchea),
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A3, Note.Duration.semicorchea),
				new Note(Note.Frequency.B3, Note.Duration.semicorchea),
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A4, Note.Duration.semicorchea),
				new Note(Note.Frequency.B4, Note.Duration.semicorchea),
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A5, Note.Duration.semicorchea),
				new Note(Note.Frequency.B5, Note.Duration.semicorchea),
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.A6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.B5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.B4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.B3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.B2, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.D3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.G3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.D4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.G4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.D5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.G5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.D6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.G6, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.C7, Note.Duration.semicorchea),
				new Note(Note.Frequency.G6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.D6, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.G5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.D5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.G4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.D4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.G3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.D3, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.A2, Note.Duration.semicorchea),
				new Note(Note.Frequency.B2, Note.Duration.semicorchea),
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A3, Note.Duration.semicorchea),
				new Note(Note.Frequency.B3, Note.Duration.semicorchea),
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A4, Note.Duration.semicorchea),
				new Note(Note.Frequency.B4, Note.Duration.semicorchea),
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A5, Note.Duration.semicorchea),
				new Note(Note.Frequency.B5, Note.Duration.semicorchea),
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.A6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.B5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.B4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.B3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.B2, Note.Duration.semicorchea),
			};
		
		Note[] piano =
			{
				//new Note(Note.Frequency.SILENCE, Note.Duration.redonda*12),
				
				
				new Note(Note.Frequency.C6, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.B5, Note.Duration.blanca),
				
				new Note(Note.Frequency.D6, Note.Duration.blanca),
				
				/*1*/
				new Note(Note.Frequency.C6, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.D6, Note.Duration.negra),
				
				new Note(Note.Frequency.C6, Note.Duration.negra),
				
				new Note(Note.Frequency.B5, Note.Duration.negra),
				
				new Note(Note.Frequency.D6, Note.Duration.negra),
				/*1end*/
				
				new Note(Note.Frequency.C6, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.B5, Note.Duration.blanca),
				
				new Note(Note.Frequency.D6, Note.Duration.blanca),
				
				/*2*/
				new Note(Note.Frequency.D6, Note.Duration.corchea),
				new Note(Note.Frequency.E6, Note.Duration.corchea),
				
				new Note(Note.Frequency.C6, Note.Duration.blanca+Note.Duration.negra),
				
				
				new Note(Note.Frequency.E6, Note.Duration.negra),
				
				new Note(Note.Frequency.D6, Note.Duration.negra),
				
				new Note(Note.Frequency.C6, Note.Duration.negra),
				
				new Note(Note.Frequency.B5, Note.Duration.negra),
				/*2end*/
			};
		
		Note[] piano2 =
			{
				//new Note(Note.Frequency.SILENCE, Note.Duration.redonda*12),
				
				
				new Note(Note.Frequency.C5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.B4, Note.Duration.blanca),
				
				new Note(Note.Frequency.D5, Note.Duration.blanca),
				
				/*1*/
				new Note(Note.Frequency.C5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.F5, Note.Duration.negra),
				
				new Note(Note.Frequency.E5, Note.Duration.negra),
				
				new Note(Note.Frequency.D5, Note.Duration.negra),
				
				new Note(Note.Frequency.F5, Note.Duration.negra),
				/*1end*/
				
				new Note(Note.Frequency.C5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.B4, Note.Duration.blanca),
				
				new Note(Note.Frequency.D5, Note.Duration.blanca),
				
				/*2*/
				new Note(Note.Frequency.D5, Note.Duration.corchea),
				new Note(Note.Frequency.E5, Note.Duration.corchea),
				
				new Note(Note.Frequency.C5, Note.Duration.blanca+Note.Duration.negra),
				
				
				new Note(Note.Frequency.C6, Note.Duration.negra),
				
				new Note(Note.Frequency.B5, Note.Duration.negra),
				
				new Note(Note.Frequency.A5, Note.Duration.negra),
				
				new Note(Note.Frequency.G5, Note.Duration.negra),
				/*2end*/
			};
		
		Note[] piano3 =
			{
				//new Note(Note.Frequency.SILENCE, Note.Duration.redonda*12),
				
				
				new Note(Note.Frequency.E5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.D5, Note.Duration.blanca),
				
				new Note(Note.Frequency.F5, Note.Duration.blanca),
				
				/*1*/
				new Note(Note.Frequency.E5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.SILENCE, Note.Duration.redonda),
				/*1end*/
				
				new Note(Note.Frequency.E5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.D5, Note.Duration.blanca),
				
				new Note(Note.Frequency.F5, Note.Duration.blanca),
				
				/*2*/
				new Note(Note.Frequency.B5, Note.Duration.corchea),
				new Note(Note.Frequency.C6, Note.Duration.corchea),
				
				new Note(Note.Frequency.A5, Note.Duration.blanca+Note.Duration.negra),
				
				
				new Note(Note.Frequency.SILENCE, Note.Duration.redonda)
				/*2end*/
			};
		
		Instrument[] instruments = MidiPlayer.getInstruments();
		/*
		for(int i = 0; i < instruments.length; i++)
			System.out.println(i + ": " + instruments[i].getName());
		*/
		Conductor conductor = new Conductor(60);
		
		Set<Future<Boolean>> finished = new HashSet<Future<Boolean>>();
		ExecutorService executor = Executors.newCachedThreadPool();
		finished.add(executor.submit(new Performer(conductor, arpegio, 0, instruments[46])));
		finished.add(executor.submit(new Performer(conductor, piano, 1, instruments[1])));
		finished.add(executor.submit(new Performer(conductor, piano2, 1, instruments[1])));
		finished.add(executor.submit(new Performer(conductor, piano3, 1, instruments[1])));
		
		
		
		try {
			Thread.sleep(50);
			executor.submit(conductor);
			executor.shutdown();
			
			for(Future<Boolean> performerFinished: finished)
			{
				if(performerFinished.get())
					System.out.println("performer finished successfully.");
				else
					System.out.println("performer was interrupted.");
			}
			
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
			System.out.println("Applause.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

}
