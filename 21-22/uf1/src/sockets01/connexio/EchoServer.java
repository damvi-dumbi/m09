package sockets01.connexio;

import java.net.*;
import java.io.*;

public class EchoServer
{
    public static void main(String[] args)
    {
        
        if (args.length != 1)
        {
            System.err.println("Expected PORT as parameter");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        try
        (
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);                   
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        ) {
        	System.out.println("Connection established at port " + clientSocket.getPort()
        						+ " and local port " + clientSocket.getLocalPort());
            
        	String inputLine;
            while((inputLine = in.readLine()) != null)
            {
            	System.out.println("Received: " + inputLine);
                out.println(inputLine);
            }
        } catch (IOException e)
        {
            System.out.println("Exception caught when trying to listen to port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}