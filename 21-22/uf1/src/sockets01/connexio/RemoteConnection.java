package sockets01.connexio;

import java.io.*;
import java.net.*;

public class RemoteConnection
{
	public static void main(String[] args)
	{
		/*
		if (args.length < 1)
		{
			System.err.println("Expected URL as argument");
			System.exit(1);
		}
		*/
		
		URL url = null;
		try
		{
			url = new URL("http://ies-sabadell.cat"); //new URL(args[0]);
		} catch (MalformedURLException e) {
			System.err.println("Wrong URL format: " + e.getMessage());
			System.exit(1);
		}
		
		String hostName = url.getHost();
		int port = 80;
		
		try 
		(
			Socket socket = new Socket(hostName, port);
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		)
		{
			out.println("GET / HTTP/1.1");
    		out.println("Host: "+hostName);
            out.println();

            String answer;
            System.out.println("Received: ");
            while((answer = in.readLine()) != null)
            		System.out.println(answer);
            
            socket.close();
		} catch (UnknownHostException e) {

			System.err.println("Server not found: " + e.getMessage());
			System.exit(1);
		} catch (IOException e) {

			System.out.println("I/O error: " + e.getMessage());
			System.exit(1);
		}
	}

}
