package sockets01.connexio;

import java.io.*;
import java.net.*;

public class EchoClient
{
    public static void main(String[] args)
    {
        /*
        if (args.length != 2)
        {
            System.err.println("Expected HOSTNAME and PORT as parameter");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        */
        String hostName = "localhost";
        int portNumber = 60009;

        try
        (
            Socket echoSocket = new Socket(hostName, portNumber);
            PrintWriter out = new PrintWriter(echoSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))
        ) {
            String userInput;
            while ((userInput = stdIn.readLine()) != null)
            {
                System.out.println("Attempting to send: "+ userInput);
                System.out.println("Received: " + in.readLine());
            }
        } catch (UnknownHostException e)
        {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e)
        {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        } 
    }
}