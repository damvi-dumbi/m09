package sockets01.data;

/*
 * Copyright (c) 1995, 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

import java.io.*;
import java.net.*;
import java.util.Random;

/*
 * Modification of EchoClient to test DataStreams with types.
 * Demo code for teaching purposes. Use at your own risk.
 */

public class EchoClientTypes
{
    public static void main(String[] args)
    {
        /*
        if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }
		*/
        String hostName = "localhost";
        int portNumber = 60009;

        try (
            Socket echoSocket = new Socket(hostName, portNumber);
            DataOutputStream out = new DataOutputStream(echoSocket.getOutputStream());
        	DataInputStream in = new DataInputStream(echoSocket.getInputStream());
        	BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))
        ) {
        	Random random = new Random();
        	boolean finish = false;
        	do
        	{
	            System.out.println("Select the type to send:");
	            System.out.println("1. boolean");
	            System.out.println("2. byte");
	            System.out.println("3. char");
	            System.out.println("4. unsigned int");
	            System.out.println("5. int");
	            System.out.println("6. double");
	            System.out.println("7. string");
	            System.out.println("8. close session");
	            
	            String optionString = stdIn.readLine();
	            int option = (int) optionString.getBytes()[0]-48;
	            System.out.println(option);
	            try
	            {
	            	//sending the type to the server
	            	out.writeByte(option);
		            switch(option)
		            {
		            	case 1:
		            		boolean msgBoolean = random.nextBoolean();
		            		System.out.println("Sending: " + msgBoolean);
		            		out.writeBoolean(msgBoolean);
		            		System.out.println("Received: " + in.readBoolean());
		            		break;
		            	case 2:
		            		byte msgByte = (byte) random.nextInt(255);
		            		System.out.println("Sending: " + msgByte);
		            		out.writeByte(msgByte);
		            		System.out.println("Received: " + in.readByte());
		            		break;
		            	case 3:
		            		char msgChar = (char) random.nextInt(255);
		            		System.out.println("Sending: " + msgChar);
		            		out.writeChar(msgChar);
		            		System.out.println("Received: " + in.readChar());
		            		break;
		            	case 4:
		            		int msgUInt = random.nextInt(65535);
		            		System.out.println("Sending: " + msgUInt);
		            		out.writeInt(msgUInt);
		            		System.out.println("Received: " + in.readInt());
		            		break;
		            	case 5:
		            		int msgInt = random.nextInt();
		            		System.out.println("Sending: " + msgInt);
		            		out.writeInt(msgInt);
		            		System.out.println("Received: " + in.readInt());
		            		break;
		            	case 6:
		            		double msgDouble = random.nextDouble();
		            		System.out.println("Sending: " + msgDouble);
		            		out.writeDouble(msgDouble);
		            		System.out.println("Received: " + in.readDouble());
		            		break;
		            	case 7:
		            		String msgString = "";
		            		for (int i = 0; i < 100; i++)
		            		{
		            			msgString+=(char)(random.nextInt(106)+20);
							}
		            		System.out.println("Sending a 100 length string: " + msgString);
		            		int msgLength = msgString.length();
		            		out.writeInt(msgLength);
		            		out.writeBytes(msgString);
		            		int answerLength = in.readInt();
		            		System.out.println("Answer length: " + answerLength);
		            		byte[] answer = new byte[answerLength];
		            		int readBytes = 0;
		            		int currentAnswerLength = answerLength;
		            		while(readBytes < answerLength)
		            		{
		            			System.out.println(readBytes + " - " + currentAnswerLength);
		            			int currentReadBytes = in.read(answer,readBytes,currentAnswerLength);
		            			currentAnswerLength -= currentReadBytes;
		            			readBytes += currentReadBytes;
		            			System.out.println("Read a chunk of " + currentReadBytes + " bytes.");
		            		}
		            		System.out.println("Received: " + new String(answer));
		            		break;
		            	case 8:
		            	default:
		            		finish = true;
		            		int closingAnswer = in.readInt();
		            		System.out.print("Received : " + closingAnswer + " Closing connection.");
		            }
	            }catch (IOException e) {
	            	System.out.println("Problem reading/sending the input data.");
	            	e.printStackTrace();
	            }
        	}while(!finish);
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        } catch (Exception e) {
        	System.out.println("Another exception.");
        	e.printStackTrace();
        	System.exit(-1);
        }
    }
}