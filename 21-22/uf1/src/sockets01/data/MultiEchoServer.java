package sockets01.data;

import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.*;

public class MultiEchoServer
{
	private static class ClientHandler implements Runnable
	{
		Socket socket;
		PrintWriter out;                   
        BufferedReader in;
        
        public ClientHandler(Socket socket) throws IOException
        {
        	this.socket = socket;
        	out = new PrintWriter(socket.getOutputStream(), true);                   
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }
        
		public void run()
		{
			try 
			{
				System.out.println(Thread.currentThread().getName() + ": Connection established with "+ socket.getInetAddress() +" at port " + socket.getPort()
				+ " and local port " + socket.getLocalPort());
	
				String inputLine;
				while((inputLine = in.readLine()) != null)
				{
					System.out.println("Received: " + inputLine);
					out.println(inputLine);
				}
				
			} catch (IOException e)
	        {
	            System.out.println("Exception caught when trying to listen to port "
	                + socket.getLocalPort() + " or listening for a connection");
	            System.out.println(e.getMessage());
	        } finally {
	        	try
	        	{
					out.close();
					in.close();
					socket.close();
	        	} catch (IOException e)
		        {
	        		System.out.println(e.getMessage());
		        }
	        }
		}
		
	}
	
    public static void main(String[] args)
    {
        /*
        if (args.length != 1)
        {
            System.err.println("Expected PORT as parameter");
            System.exit(1);
        }
        */
    	
        int portNumber = 60009;
        
        try
        (
            ServerSocket serverSocket = new ServerSocket(portNumber);
        ) {
        	ExecutorService executors = Executors.newCachedThreadPool();
        	while(true)
        	{
        		System.out.println("Esperant que es connecti un client");
        		Socket clientSocket = serverSocket.accept();
        		executors.execute(new ClientHandler(clientSocket));
        	}
        } catch (IOException e)
        {
            System.out.println("Exception caught when trying to listen to port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}