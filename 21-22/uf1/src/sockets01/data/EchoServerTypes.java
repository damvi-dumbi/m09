package sockets01.data;

/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.net.*;
import java.io.*;

/*
 * Modification of EchoServer to test DataStreams with types.
 * Demo code for teaching purposes. Use at your own risk.
 */

public class EchoServerTypes
{
    public static void main(String[] args)
    {
        /*
        if (args.length != 1)
        {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        */
    	int portNumber = 60009;
        try
        (
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
        	DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());                   
        	DataInputStream in = new DataInputStream(clientSocket.getInputStream());
        ) {
        	System.out.println("Connection established at port " + clientSocket.getPort()
        						+ " and local port " + clientSocket.getLocalPort());
            boolean finish = false;
            do
            {
            	try
	            {
            		int option = in.readByte();
		            switch(option)
		            {
		            	case 1:
		            		boolean msgBoolean = in.readBoolean();
		            		System.out.println("Received: " + msgBoolean);
		            		out.writeBoolean(msgBoolean);
		            		break;
		            	case 2:
		            		byte msgByte = in.readByte();
		            		System.out.println("Received: " + msgByte);
		            		out.writeByte(msgByte);
		            		break;
		            	case 3:
		            		char msgChar = in.readChar();
		            		System.out.println("Received: " + msgChar);
		            		out.writeChar(msgChar);
		            		break;
		            	case 4:
		            		int msgUInt = in.readInt();
		            		System.out.println("Received: " + msgUInt);
		            		out.writeInt(msgUInt);
		            		break;
		            	case 5:
		            		int msgInt = in.readInt();
		            		System.out.println("Received: " + msgInt);
		            		out.writeInt(msgInt);
		            		break;
		            	case 6:
		            		double msgDouble = in.readDouble();
		            		System.out.println("Received: " + msgDouble);
		            		out.writeDouble(msgDouble);
		            		break;
		            	case 7:
		            		int msgLength = in.readInt();
		            		System.out.println("Receiving a " + msgLength + " length string.");
		            		byte[] answer = new byte[msgLength];
		            		int readBytes = 0;
		            		int currentMsgLength = msgLength;
		            		while(readBytes < msgLength)
		            		{
		            			System.out.println(readBytes + " - " + currentMsgLength);
		            			int currentReadBytes = in.read(answer,readBytes,currentMsgLength);
		            			currentMsgLength -= currentReadBytes;
		            			readBytes += currentReadBytes;
		            			System.out.println("Read a chunk of " + currentReadBytes + " bytes.");
		            		}
		            		String answerString = new String(answer);
		            		System.out.println("Received: " + answerString);
		            		out.writeInt(answerString.length());
		            		out.writeBytes(answerString);
		            		break;
		            	case 8:
		            	default:
		            		out.writeInt(option);
		            		finish = true;
		            		System.out.print("Closing connection.");
		            }
	            }catch (IOException e) {
	            	System.out.println("Problem reading/sending the input data.");
	            	e.printStackTrace();
	            	finish = true;
	            }
            }while(!finish);
        } catch (IOException e)
        {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}
