package threads02;

public class BaseDeDades
{
	private Integer m_ValorEnter = 0;
	
	//sincronitzem el mètode, de forma que només un thread pugui utilitzar-lo.
	//val a dir que l'objecte per sincronitzar és la classe.
	public synchronized Integer GetEnter()
	{
		return m_ValorEnter;
	}
	
	//sincronitzem el mètode, de forma que només un thread pugui utilitzar-lo.
	//val a dir que l'objecte per sincronitzar és la classe.	
	public synchronized void SetEnter(int valor)
	{
		m_ValorEnter = valor;
		System.out.println("Enter amb valor: " + m_ValorEnter);
	}

}
