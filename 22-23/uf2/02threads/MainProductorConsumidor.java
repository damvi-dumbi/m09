package threads02;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainProductorConsumidor
{
	public static void main(String[] args)
	{
		ExecutorService executor = Executors.newCachedThreadPool();
		
		Productor productor = new Productor();
		
		for(int i = 0; i < 50; ++i)
		{
			executor.execute(new Consumidor(productor));
		}
		
		executor.execute(productor);
		
		executor.shutdown();
		
		try
		{
			if(!executor.awaitTermination(1, TimeUnit.SECONDS))
			{
				executor.shutdownNow();
				
				if(!executor.awaitTermination(30, TimeUnit.MILLISECONDS))
					System.err.println("Haurien d'haver acabat");
			}
			
		} catch (InterruptedException e) {
			System.err.println("No hauria de passar");
		}
		
		System.out.println("Finiquitado");
	}
}
