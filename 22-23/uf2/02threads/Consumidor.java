package threads02;

public class Consumidor implements Runnable
{
	
	Productor m_Productor;
	
	public Consumidor(Productor productor)
	{
		m_Productor = productor;
	}

	public void run()
	{
		try {
			while(true)
			{
				//esperarem que hi hagi dades a consumir
				synchronized(m_Productor)
				{
					m_Productor.wait();
				}
				
				//això pot estar aquí si sabem que VullConsumir és sincronitzat, si no
				//hem de fer-ho dins de la zona sincronitzada anterior
				if(!m_Productor.VullConsumir())
					continue;
				
				System.out.println(Thread.currentThread().getId() + ": " + m_Productor.GetValue());
			}
		} catch (InterruptedException e) {
			System.err.println("Acabant thread" + Thread.currentThread().getId());
		}
	}

}
