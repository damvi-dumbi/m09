package threads02;

public class Productor implements Runnable
{
	int m_Value;
	int m_Queue;
	
	//o aquest mètode és sincronitzat i per tant, només un thread a la vegada
	//sabrà si pot consumir
	//em permeto enviar el throw perquè de fet vull que el consumidor rebi l'excepció
	//per tal d'acabar-se
	public synchronized boolean VullConsumir() throws InterruptedException
	{
		if(m_Queue > 0)
		{
			Thread.sleep(1);
			m_Queue--;
			System.out.println(Thread.currentThread().getName() + ": " + m_Queue);
			return true;
		}
		
		return false;
	}
	
	public int GetValue()
	{
		return m_Value;
	}

	public void run()
	{
		try {
			m_Value = 0;
			//màxim de consumidors que tindrem
			m_Queue = 6;
			while(true)
			{
				//simulem que esperem dades
				Thread.sleep((long) (Math.random()*10));
				
				m_Value = (int)(Math.random()*100);
				
				synchronized(this)
				{
					//de nou tenim 6 posicions de consum
					m_Queue = 6;
					//avisem a TOTS els consumidors
					this.notifyAll();
				}
			
			}
		} catch (InterruptedException e) {
			System.err.println("Acabant thread" + Thread.currentThread().getId());
		}
	}
}
