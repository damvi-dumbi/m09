package threads02;

public class Sumador implements Runnable
{
	private BaseDeDades m_BaseDades;
	
	
	public Sumador(BaseDeDades bd)
	{
		this.m_BaseDades = bd;
	}

	public void run()
	{
		//utilitzem un objecte com a sincronitzador.
		//aquest objecte ha de ser comú a tothom que necessiti d'aquest
		//punt de sincronia.
		synchronized(m_BaseDades)
		{
			int valor = m_BaseDades.GetEnter();
			System.out.println(Thread.currentThread().getId() + " calculating.");
			m_BaseDades.SetEnter(++valor);
		}
	}

}
