package threads01;

import java.util.ArrayList;

public class ElMeuTest {

	
	
	public static void main(String[] args)
	{
		System.out.println("Vaig a calcular un factorial!");
		
		//hay que hacer un thread, cómo será?
		ArrayList<CalculadorDeFactorial> llistaDeThreads = new ArrayList<CalculadorDeFactorial>();
		
		for(int i = 1; i < 30; ++i)
			llistaDeThreads.add(new CalculadorDeFactorial(i));
		
		
		System.out.println("Executo els thread");
		
		for(int i = 0; i < llistaDeThreads.size(); ++i)
			llistaDeThreads.get(i).start();
		
		System.out.println("He executat els threads");
		
		try {
			for(int i = 0; i < llistaDeThreads.size(); ++i)
				llistaDeThreads.get(i).join();
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		System.out.println("El resultat és: " + llistaDeThreads);
	}
}