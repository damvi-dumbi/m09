package threads01;

public class CalculadorDeFactorial extends Thread
{
	private int factorial;
	private long resultat;
	
	public CalculadorDeFactorial(int factorial)
	{
		this.factorial = factorial;
	}
	
	private long factorial(long factor)
	{
		if(factor == 1)
			return 1;
		else
			return factor * factorial(factor -1);
	}
	
	public void run()
	{
		resultat = factorial(factorial);
		System.out.println("He acabat de calcular" + Thread.currentThread().getName());
		
    }
	
	public long getResultat()
	{
		return resultat;
	}
	
	public String toString()
	{
		return "factorial de " + factorial + " és: " + getResultat();
	}
}








