package threads01;

public class CalculadorDeFactorialRunnable implements Runnable
{
	private int factorial;
	private long resultat;
	
	public CalculadorDeFactorialRunnable(int factorial)
	{
		this.factorial = factorial;
	}
	
	private long factorial(long factor)
	{
		if(factor == 1)
			return 1;
		else
			return factor * factorial(factor -1);
	}
	
	public void run()
	{
		try
		{
			resultat = factorial(factorial);
			
			Thread.sleep((long) (Math.random()*5000));
			System.out.println(Thread.currentThread().getName() + ": He acabat de calcular");
			
		} catch (InterruptedException e) {
			System.out.println(Thread.currentThread().getName() + ": M'han interromput.");
		}
    }
	
	public long getResultat()
	{
		return resultat;
	}
	
	public String toString()
	{
		return "factorial de " + factorial + " és: " + getResultat();
	}
}








