package threads01;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ElMeuTestAmbCallable
{
	public static void main(String[] args)
	{
		ExecutorService executor = Executors.newCachedThreadPool();
		ArrayList<Future<Long>> futurs = new ArrayList<Future<Long>>();
		
		for(int i = 1; i < 30; ++i)
			futurs.add(executor.submit(new ElMeuCallable(i)));
		
		executor.shutdown();
		
		
		try {
			for(Future<Long> futur : futurs)
				System.out.println("El resultat del factorial és: " +futur.get());
			
			//A Brian le gusta hacer cosas raras
			Iterator iterator = futurs.iterator();
			while(iterator.hasNext())
			{
				Future<Long> futur = (Future<Long>) iterator.next();
				System.out.println("El resultat del factorial és: " +futur.get());
				iterator.remove();
			}
			
		} catch (InterruptedException e) {
			System.out.println("Això dificilment hauria de passar, vol dir que ens han interromput el main.");
		}catch (ExecutionException e) {
			System.out.println("Això dificilment hauria de passar, vol dir que ens han interromput el main.");
		}
	}

}
