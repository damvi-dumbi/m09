package threads01;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ElMeuExecutorTest
{
	public static void main(String[] args)
	{
		ExecutorService executor = Executors.newCachedThreadPool();
		
		for(int i = 1; i < 30; ++i)
			executor.execute(new CalculadorDeFactorialRunnable(i));
		
		executor.shutdown();
		
		try {
			if(!executor.awaitTermination(3, TimeUnit.SECONDS))
			{
				System.out.println("han passat els 5 segons, però hi ha gent encara fent feina.");
				executor.shutdownNow();
			}
		} catch (InterruptedException e) {
			System.out.println("Això dificilment hauria de passar, vol dir que ens han interromput el main.");
		}
		
	}

}
