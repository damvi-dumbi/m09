package threads01;

import java.util.concurrent.Callable;

public class ElMeuCallable implements Callable<Long>
{
	int factorial;
	
	public ElMeuCallable(int factorial)
	{
		this.factorial = factorial;
	}
	
	private long factorial(long factor)
	{
		if(factor == 1)
			return 1;
		else
			return factor * factorial(factor -1);
	}

	public Long call()
	{
		return factorial(factorial);
	}

}
